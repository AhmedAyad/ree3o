﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_Facebook.Data;
using MVC_Facebook.Models;
using MVC_Facebook.Models.Repository;

namespace MVC_Facebook.Controllers
{
    [Authorize(Roles = "Normal User")]
    public class PostsController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;
        private readonly ILikeRepository _likeRepository;
        private readonly UserManager<User> _userManager;

        public PostsController(IPostRepository postRepository, UserManager<User> userManager,  IUserRepository userRepository, ILikeRepository likeRepository)
        {
            _postRepository = postRepository;
            _userManager = userManager;
            _userRepository = userRepository;
            _likeRepository = likeRepository;
        }

        [HttpGet]
        public IActionResult ProfilePosts(string id)
        {
            var user = _userRepository.GetByIdWithEagerLoading(id);
            var ListOfPosts = user.Posts.OrderByDescending(p => p.TimeStamp).ToList();
            return PartialView("PostsList", ListOfPosts);

        }

        // GET: Posts
        public IActionResult NewsFeed()
        {
            var userId = _userManager.GetUserId(User);
            var user = _userRepository.GetByIdWithEagerLoading(userId);
            var result = user.Posts.ToList();

            foreach (var item in user.Friends)
            {
                var tempPost = item.Receiver.Posts.Where(p => p.IsDeleted == false).ToList();
                if (!(tempPost is null))
                    result.AddRange(tempPost);
            }
            foreach (var item in user.FriendOf)
            {
                var tempPost = item.Sender.Posts.Where(p => p.IsDeleted == false).ToList();
                if (!(tempPost is null))
                    result.AddRange(tempPost);
            }
            var r = result.OrderByDescending(p => p.TimeStamp);
            return PartialView("PostsList", r);
        }

        [HttpPost]
        public IActionResult PostLike(int PostID, string LikeOwnerID, bool isDeleted)
        {
            _likeRepository.Add(new Like() { PostID = PostID, LikeOwnerID = LikeOwnerID, IsDeleted = isDeleted });
            return RedirectToAction("Index", "Home");
        }

        //GET: /Posts/ShowLikeOwners/PostID
        [HttpGet]
        public IActionResult ShowLikeOwners(int PostID)
        {
            //Step1: get all likes list by postID
            var likesList = _likeRepository.GetAll().Where(l => l.PostID == PostID && l.IsDeleted == false);
            //Step2: get all users by likeOwnerID in each item in like list derived from prev step
            var likeOwners = from User in _userRepository.GetAll()
                             from Like in likesList
                             where User.Id == Like.LikeOwnerID
                             select User;

            //try getting user's(SignedInUser) friends
            var currentUserID = _userManager.GetUserId(User);
            var user = _userRepository.GetByIdWithEagerLoading(currentUserID);
            ViewData["User"] = user;
            var allFriends = user.Friends.Select(f => f.Receiver).Union(user.FriendOf.Select(f => f.Sender));

            ViewData["AllFriends"] = allFriends;

            //Step3:return user model to the partial view
            return PartialView(likeOwners.ToList());
        }

        // GET: Posts/Create
        public IActionResult Create()
        {
            return PartialView();
        }

        // POST: Posts/Create
        [HttpPost]
        public JsonResult Create(string Body)
        {
            Post post = new Post();
            post.PostOwnerID = _userManager.GetUserId(User);
            post.Body = Body;
            _postRepository.Add(post);
            return Json("");
        }

        // GET: Posts/Delete/5
        public IActionResult Delete(int id)
        {
            var post = _postRepository.GetById(id);
            if (post == null)
            {
                return NotFound();
            }
            _postRepository.Delete(id);
            return Ok();
        }
           }
}
