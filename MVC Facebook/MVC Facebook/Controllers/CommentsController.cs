﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_Facebook.Data;
using MVC_Facebook.Models;
using MVC_Facebook.Models.Repository;

namespace MVC_Facebook.Controllers
{
    [Authorize(Roles = "Normal User")]
    public class CommentsController : Controller
    {
        private readonly ICommentRepository _commentRepository;

        public CommentsController(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        [HttpGet]
        public IActionResult showCommentPost(int id)
        {
            List<Comment> comments = _commentRepository.GetAll().Where(c => c.PostID == id && c.IsDeleted == false).Include(p => p.CommentOwner).OrderBy(c=>c.TimeStamp).ToList();
            return PartialView(comments);

        }

        public JsonResult AddComment([Bind("ID,Body,PostID,CommentOwnerID,IsDeleted")] Comment com)
        {
            string CurrentUserID = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            com.CommentOwnerID = CurrentUserID;
            _commentRepository.Add(com);
            return Json(com.PostID);
        }

        public JsonResult Delete(int id)
        {
            Comment comment = _commentRepository.GetById(id);
            if (comment == null)
            {
                return Json("");
            }
            comment = _commentRepository.Delete(id);
            return Json(comment.PostID);
        }

    }
}
