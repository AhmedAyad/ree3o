﻿using Microsoft.EntityFrameworkCore;
using MVC_Facebook.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Facebook.Models.Repository
{
    public class LikeRepository : ILikeRepository
    {
        readonly ApplicationDbContext _context;
        public LikeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Like like)
        {
            var obj = _context.Likes.FirstOrDefault(l => l.LikeOwnerID == like.LikeOwnerID && l.PostID == like.PostID);
            if (obj == null)
            {
                _context.Likes.Add(like);
                _context.SaveChanges();
            }
            else
            {
                obj.IsDeleted = like.IsDeleted;
                Update(obj);
            }
        }

        public IQueryable<Like> GetAll()
        {
            return _context.Likes;
        }

        public void Update(Like Object)
        {
            _context.Entry(Object).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }

    public interface ILikeRepository
    {
        void Add(Like like);
        IQueryable<Like> GetAll();
    }
}
