﻿using MVC_Facebook.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MVC_Facebook.Models.Repository
{
    public interface ICommentRepository
    {
        public IQueryable<Comment> GetAll();
        public Comment GetById(int id);
        public void Add(Comment Object);
        public Comment Delete(int id);
    }

    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;
        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Comment Object)
        {
            _context.Comments.Add(Object);
            _context.SaveChanges();
        }

        public Comment Delete(int id)
        {
            Comment comment = _context.Comments.Find(id);
            comment.IsDeleted = true;
            _context.Comments.Update(comment);
            _context.SaveChanges();
            return comment;
        }

        public IQueryable<Comment> GetAll()
        {
            return _context.Comments;
        }

        public Comment GetById(int id)
        {
            Comment comment = _context.Comments.Find(id);
            return comment;
        }

    }
}
